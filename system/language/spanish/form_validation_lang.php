<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author  CodeIgniter community
 * @author  Iban Eguia
 * @copyright   Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license http://opensource.org/licenses/MIT  MIT License
 * @link    https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']       = 'El campo {field} es obligatorio.';
$lang['form_validation_isset']          = 'El campo {field} debe contener un valor.';
$lang['form_validation_valid_email']        = 'El campo {field} debe contener un email válido.';
$lang['form_validation_valid_emails']       = 'El campo {field} debe contener todos los emails válidos.';
$lang['form_validation_valid_url']      = 'El campo {field} debe contener una URL válida.';
$lang['form_validation_valid_ip']       = 'El campo {field} debe contener una IP válida.';
$lang['form_validation_min_length']     = 'El campo {field} debe ser de al menos {param} caracteres de longitud.';
$lang['form_validation_max_length']     = 'El campo {field} no puede superar los {param} caracteres de longitud.';
$lang['form_validation_exact_length']       = 'El campo {field} debe ser de exactamente {param} caracteres de longitud.';
$lang['form_validation_alpha']          = 'El campo {field} solo puede contener caracteres alfabéticos.';
$lang['form_validation_alpha_numeric']      = 'El campo {field} solo puede contener caracteres alfanuméricos.';
$lang['form_validation_alpha_numeric_spaces']   = 'El campo {field} solo puede contener caracteres alfanuméricos y espacios.';
$lang['form_validation_alpha_dash']     = 'El campo {field} solo puede contener caracteres alfanuméricos, guiones bajos y guiones.';
$lang['form_validation_numeric']        = 'El campo {field} solo puede contener números.';
$lang['form_validation_is_numeric']     = 'El campo {field} solo puede contener caracteres numéricos.';
$lang['form_validation_integer']        = 'El campo {field} debe contener un entero.';
$lang['form_validation_regex_match']        = 'El campo {field} no está en el formato correcto.';
$lang['form_validation_matches']        = 'El campo {field} no coincide con el campo {param}.';
$lang['form_validation_differs']        = 'El campo {field} debe ser distinto al campo {param}.';
$lang['form_validation_is_unique']      = 'El campo {field} debe contener un valor único.';
$lang['form_validation_is_natural']     = 'El campo {field} solo puede contener dígitos.';
$lang['form_validation_is_natural_no_zero'] = 'El campo {field} solo puede contener dígitos y debe ser mayor que cero.';
$lang['form_validation_decimal']        = 'El campo {field} debe contener un número decimal.';
$lang['form_validation_less_than']      = 'El campo {field} debe contener un número menor que {param}.';
$lang['form_validation_less_than_equal_to'] = 'El campo {field} debe contener un número menor o igual a {param}.';
$lang['form_validation_greater_than']       = 'El campo {field} debe contener un número mayor que {param}.';
$lang['form_validation_greater_than_equal_to']  = 'El campo {field} debe contener un número mayor o igual a {param}.';
$lang['form_validation_error_message_not_set']  = 'No se ha podido acceder al mensaje de error correspondiente para el campo {field}.';
$lang['form_validation_in_list']        = 'El campo {field} debe contener uno de estos: {param}.';

// ADDED WITH MY_Form_validation.php
$lang['form_validation_file_required'] = "El campo {field} es obligatorio.";
$lang['form_validation_file_size_max'] = "El archivo del campo {field} es demasiado grande (el máximo permitido es {param}).";
$lang['form_validation_file_size_min'] = "El archivo del campo {field} es demasiado pequeño (el mínimo permitido es {param}).";
$lang['form_validation_file_allowed_type'] = "El archivo del campo {field} debería ser del tipo {param}.";
$lang['form_validation_file_disallowed_type']  = "El archivo del campo {field} no puede ser un {param}.";
$lang['form_validation_file_image_maxdim'] = "El archivo del campo {field} es demasiada grande.";
$lang['form_validation_file_image_mindim'] = "El archivo del campo {field} es demasiada pequeño.";
$lang['form_validation_file_image_exactdim'] = "El archivo del campo {field} no tiene las dimensiones adecuadas.";
$lang['form_validation_is_exactly'] = "El campo {field} contiene un valor no válido.";
$lang['form_validation_is_not'] = "El campo {field} contiene un valor no válido.";
$lang['form_validation_valid_hour'] = "El campo {field} ha de contener una hora válida.";
$lang['form_validation_error_max_filesize_phpini'] = "El archivo supera la directiva upload_max_filesize de php.ini.";
$lang['form_validation_error_max_filesize_form'] = "El archivo supera la directiva MAX_FILE_SIZE especificada en el formulario HTML.";
$lang['form_validation_error_partial_upload'] = "El archivo sólo se ha subido parcialmente.";
$lang['form_validation_error_temp_dir'] = "Error del directorio temporal.";
$lang['form_validation_error_disk_write'] = "Error de escritura en disco.";
$lang['form_validation_error_stopped'] = "Subida del archivo detenida por extensión.";
$lang['form_validation_error_unexpected'] = "Error inesperado de subida de archivo. Error: {field}";
$lang['form_validation_valid_date'] = "El campo {field} ha de contener una fecha válida.";
$lang['form_validation_valid_range_date'] = "El campo {field} ha de contener dos fechas válidas.";
$lang['form_validation_valid_geopos'] = "El mapa %s no tiene una posición correcta";
$lang['form_validation_valid_multiple_geopos'] = "El mapa %s no tiene una posición correcta";
$lang['form_validation_valid_recaptcha'] = "El captcha de validación no es correcto";
