<?php 

class MY_Model extends CI_Model {

    public $errors = array();
    public $ci;
    public $table;

    public function __construct() 
    {
        parent::__construct();
        $this->ci =& get_instance();
    }

    public function insert($data, $table) 
    {
        $this->load->database();

        try 
        {
            $affected_row = $this->db->insert($table, $data);
            
            if ($affected_row) 
            {
                return $this->db->insert_id();
            }
        }
        catch (Exception $e) 
        {
            $this->set_exception($e); 
        }

        return FALSE;
    }

    public function insert_batch($data, $table) 
    {
        $this->load->database();

        try 
        {
            $affected_rows = $this->db->insert_batch($table, $data);
            
            if ($affected_rows) 
            {
                return TRUE;
            }
        }
        catch (Exception $e) 
        {
            $this->set_exception($e); 
        }

        return FALSE;
    }

    public function update_batch($data, $table, $primary_key = 'id')
    {
        $this->load->database();

        try 
        {
            return $this->db->update_batch($table, $data, $primary_key);
        }
        catch (Exception $e) 
        {
            $this->set_exception($e); 
        }

        return FALSE;
        
    }


    public function update($data, $id, $table)
    {
        $this->load->database();

        try 
        {
            $affected_row = $this->db->update($table, $data, array('id' => $id));  

            if ($affected_row)
            {
                return TRUE;
            } 
        } 
        catch (Exception $e) 
        {
            $this->set_exception($e);
        }

        return FALSE;
    }

    public function delete($id, $table)
    {
        $this->load->database();

        try 
        {
            $this->db->where('id', $id);
            $this->db->delete($table);
            return $this->db->affected_rows() > 0;
        }
        catch (Exception $e) 
        {
            $this->set_exception($e);
            return FALSE;
        }
    }

    public function delete_by($where, $table)
    {
        $this->load->database();

        try 
        {
            $this->db->where($where);
            $this->db->delete($table);
            return $this->db->affected_rows() > 0;
        }
        catch (Exception $e) 
        {
            $this->set_exception($e);
            return FALSE;
        }
    }



    public function upload_file($input_name, $path = NULL)
        {
            if ($path === NULL) return FALSE;
            
            $this->load->library('upload');
            
            /* 
                Estos son los datos que retorna $this->upload->data();
                  'file_name' => string 'arzion1024.png' (length=14)
                  'file_type' => string 'image/png' (length=9)
                  'file_path' => string '/home/farmaciaojeda/writable/uploads/news/' (length=42)
                  'full_path' => string '/home/farmaciaojeda/writable/uploads/news/arzion1024.png' (length=56)
                  'raw_name' => string 'arzion1024' (length=10)
                  'orig_name' => string 'arzion1024.png' (length=14)
                  'client_name' => string 'arzion1024.png' (length=14)
                  'file_ext' => string '.png' (length=4)
                  'file_size' => float 19.64
                  'is_image' => boolean true
                  'image_width' => int 1024
                  'image_height' => int 1024
                  'image_type' => string 'png' (length=3)
                  'image_size_str' => string 'width="1024" height="1024"' (length=26)
            */
            
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png|JPG|PNG|GIF';
            $config['overwrite'] = FALSE;

            // TODO cotejar esto con la configuración del servidor
            $config['max_size'] = 1024 * 10; 
            
            // $config['max_width'] = 1024;
            // $config['max_height'] = 1024;                
            
            // TODO pensar si queremos 
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config, TRUE);

            if ($this->upload->do_upload($input_name)) 
            {
                return $this->upload->data();
            }
            // Agregamos el error al array de errores y retornamos FALSE
            $this->set_error($input_name, $this->upload->display_errors('', ''), 'input');
            return FALSE;
        }

    public function send_email($template, $subject, $from, $from_name, $to, $data = array(), $bcc = FALSE)
    {
        $this->load->library('email');
        $this->load->library('parser');
        $this->email->reply_to($from, $from_name);
        $this->email->from($from, $from_name);
        $this->email->to($to);
        
        if ($bcc)
        {
            $this->email->bcc($bcc);
        }
        $this->email->subject($subject);
        // preparo el cuerpo del email -> $body
        $body = $this->parser->parse($template,$data,TRUE);
        
        $this->email->message($body);
        
        $response = $this->email->send();
        
        if(!$response)
        {
            log_message('error',$this->email->print_debugger(array('headers','subject','body')));
            return false;
        }

        return true;
    }

    public function get_errors()
    {
        return $this->errors;
    }

    public function set_error($code, $message, $code_label = 'code')
    {
        $this->errors[] = array( $code_label => $code, 'message' => $message);
    }

    public function set_exception($e)
    {
        $this->errors[] = array('code' => $e->getCode(), 'message' => $e->getMessage());
    }


/*
|--------------------------------------------------------------------------
| Funciones de paginación
|--------------------------------------------------------------------------
*/

    /**
     * @abstract Check de paginator page number through passed params
     * @param int $page_number
     * @param  int|FALSE $last_page_number
     */
    public function check_page($page_number, $last_page_number = FALSE)
    {
        if ( is_numeric($page_number) )
        {
            if ( $last_page_number )
            {
                return (abs($page_number) > $last_page_number) ? 1 : ($page_number === 0) ? 1 : abs($page_number);
            }
            else
            {
                return ($page_number === 0) ? 1 : abs($page_number);
            }
        }
        else
        {
            return 1;
        }
    }

    public function get_paginator($options)
    {

        $default = array("page" => 1, "rows_per_page" => 10, "last_page" => 15);
        $options = array_merge($default, $options);

        $result["total_rows"] = (! $options["total_rows"]) ? 0 : ((int) $options["total_rows"] > (int) $options["rows_per_page"] ? (int) $options["total_rows"] - 1 : (int) $options["total_rows"]);
        $result["prev"] = (int) $options["page"] > 1 ? (int) $options["page"] - 1 : FALSE;
        $result["current"] = (int) $options["page"];
        $result["rows_per_page"] = $options["rows_per_page"];
        $result["next"] = FALSE;
        $result["next_link"] = FALSE;
        $result["prev_link"] = FALSE;

        // definimos un array para parsear el query_string
        $query_string = array();
        parse_str($_SERVER['QUERY_STRING'], $query_string);

        if ( (int) $options["total_rows"] > (int) $options["rows_per_page"] && (int) $options["page"]  < (int) $options["last_page"] )
        {
            $result["next"] = (int) $options["page"] + 1;
            $query_string['p'] = $result["next"];
            $result["next_link"] = current_url().'?'.http_build_query($query_string);
        }

        if ($result["prev"])
        {
            $query_string['p'] = $result["prev"];
            $result["prev_link"] = current_url().'?'.http_build_query($query_string);
        }
        
        return $result;
    }

    public function truncate($table)
    {
        $this->load->database();
        return $this->db->truncate($table);
    }

    public function trans_begin()
    {
        $this->load->database();
        return $this->db->trans_begin();
    }

    public function trans_status()
    {
        $this->load->database();
        return $this->db->trans_status();
    }

    public function trans_rollback()
    {
        $this->load->database();
        return $this->db->trans_rollback();
    }

    public function trans_commit()
    {
        $this->load->database();
        return $this->db->trans_commit();
    }

        /**
     * Run validation on the passed data
     * @param  [type] $data  [description]
     * @param  [type] $rules [description]
     * @return [type]        [description]
     */
    public function validate($data, $rules)
    {
        $this->load->library('form_validation');
        $this->form_validation->reset_validation();
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
        else
        {
            return $this->form_validation->posted_data();
        }
    }

    /**
     * [now description]
     * @return [type] [description]
     */
    public function now($time = TRUE)
    {
        return ($time) ? date("Y-m-d H:i:s") : date("Y-m-d");
    }

}