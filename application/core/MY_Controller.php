<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
}

class Front_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
}

class Back_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
}

class Api_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('response');
    }

    /**
     * Valido un json a través de un schema pasado como parámetro.
     * Utilizo la librería JsonSchema\Validator. En caso de JSON no valido, 
     * envío un header 400 con el json de errores. (Ingles)
     * @param  [type] $raw     json string
     * @param  array  $options [description]
     * @return [array]         json array
     */
    public function validate_json($raw, $options = array())
    {

        if ( isset($options['schema']) )
        {
            $schema = json_decode(file_get_contents(APPPATH.'schemas/'.$options['schema']));

            $json_object = json_decode($raw); 

            if ( json_last_error() )
            {
                $this->response->add_error(json_last_error(), json_last_error_msg());
                $this->response->send(400); 
            }

            // Validate
            $validator = new JsonSchema\Validator;
            $validator->check($json_object, $schema);

            if (! $validator->isValid()) 
            {
                foreach ($validator->getErrors() as $error) 
                {
                    $this->response->add_error($error['property'], $error['message'], 'input');
                }

                $this->response->send(400); 
            }

            $validated_json = json_decode($raw, TRUE); 

            return $validated_json;

        }

    }
}
