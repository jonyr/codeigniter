<?php

class Response {

    private $_count;
    private $_status;
    private $_object;
    private $_object_name = "object";
    private $_paging;
    private $_cached;
    private $_message;
    private $_elapsed_time;
    private $_debug = FALSE;
    private $_version;
    private $_errors = array();
    private $_benchmarks = array();
    private $_queries = array();

    public function __construct()
    {

    }

    public function set_count($count)
    {
        $this->_count = $count;
    }

    public function set_status($status)
    {
        $this->_status = $status;
    }

    public function set_object($object)
    {
        $this->_object = $object;
    }

    public function set_object_name($name)
    {
        $this->_object_name = $name;
    }

    public function set_paging($paging)
    {
        $this->_paging = $paging;
    }

    public function set_cached($cached)
    {
        $this->_cached = $cached;
    }

    public function add_message($message)
    {
        $this->_message = $message;
    }

    public function set_elapsed_time($time)
    {
        $this->_elapsed_time = $time;
    }

    public function add_benchmark( $key, $value)
    {
        $this->_benchmarks[] = array($key => $value);
    }

    public function add_error( $field_or_code, $message)
    {
        $this->_errors[$field_or_code] = $message;
    }

    public function add_errors( $errors )
    {
        $this->_errors = $errors;
    }

    public function add_query($query)
    {
        $this->_queries[] = $query;
    }

    public function set_debug_mode($debug_mode)
    {
        $this->_debug = $debug_mode;
    }

    public function set_version($version)
    {
        $this->_version = $version;
    }

    public function build()
    {
        $output = array();

        //$output["status"] = empty($this->_status) ? FALSE : $this->_status;

        if ( ! empty($this->_count) )
        {
            $output["found"] = $this->_count;
        }

        if ( ! empty($this->_object) )
        {
            /* remuevo el n + 1 item que se utiliza para saber si mostramos el boton de siguiente */
            if ( ! empty($this->_paging["next"]) )
            {
                array_pop($this->_object);
            }
            
            $output[$this->_object_name] = $this->_object;
        }

        if ( ! empty($this->_message) )
        {
            $output["message"] = $this->_message;
        }

        if ( ! empty($this->_paging) )
        {
            $output["paging"] = $this->_paging;
        }

        if ( ! empty($this->_cached) )
        {
            $output["cached"] = $this->_cached;
        }

        if ( ! empty($this->_version) )
        {
            $output["version"] = $this->_version;
        }        

        if ( ! empty($this->_elapsed_time) && $this->_debug )
        {
            $output["elapsed_time"] = $this->_elapsed_time;
        }

        if ( ! empty($this->_benchmarks) && $this->_debug )
        {
            $output["benchmarks"] = $this->_benchmarks;
        }

        if ( ! empty($this->_queries) && $this->_debug )
        {
            $output["queries"] = $this->_queries;
        }

        if ( ! empty($this->_errors) )
        {
            $output["errors"] = $this->_errors; 
            $output["errors_count"] = count($this->_errors);  
        }

        return $output;
    }

    public function send($status_code = 200)
    {
        http_response_code($status_code);
        header('Content-Type: application/json');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
        
        if (ENVIRONMENT === 'development' || $_GET['pretty']) 
        {
            echo json_encode($this->build(TRUE), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            exit();
        }

        echo json_encode($this->build(TRUE));
        exit();
    }

}