<?php

class Metatags 
{
    
    private $_h1 = "";
    private $_title = "Guia de La Plata";
    private $_charset = "text/html;charset=UTF-8";
    private $_meta_charset = NULL;
    private $_css = "text/css";
    private $_language = "es-AR";
    private $_canonical = "";
    private $_amp_canonical = "";
    private $_meta = array();
    private $_meta_prop = array();
    private $_meta_http_equiv = array();
    private $_link = array();

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->config('metatags');

        $this->set_meta_http_equiv('content-type', $this->ci->config->item('mt_content_type'));
        $this->set_title($this->ci->config->item('mt_title'));
        $this->set_meta('viewport', $this->ci->config->item('mt_viewport'));
        $this->set_meta('robots', $this->ci->config->item('mt_robots'));
        $this->set_meta('googlebot', $this->ci->config->item('mt_googlebot'));
        $this->set_meta('country', $this->ci->config->item('mt_country'));
        $this->set_meta('language', $this->ci->config->item('mt_language'));

    }

    public function set_common_meta($title, $description, $keywords)
    {
        $this->set_meta("description", $description);
        $this->set_meta("keywords", $keywords);
        $this->set_title($title);
    }

    public function set_title($title)
    {
        $this->_title = $title;
    }
    
    public function append_title($title)
    {
        $this->_title .= " - {$title}";
    }

    public function prepend_title($title)
    {
        $this->_title = "{$title} - {$this->_title}";
    }

    public function set_h1($text)
    {
        $this->_h1 = $text;
    }

    public function set_meta($name, $content)
    {
        $this->_meta[$name] = $content;
        return TRUE;
    }

    public function set_meta_http_equiv($http_equiv, $content)
    {
        $this->_meta_http_equiv[$http_equiv] = $content;
        return TRUE;
    }

    public function reset_meta_http_equiv()
    {
        $this->_meta_http_equiv = array();
    }

    public function remove_meta($name)
    {
        unset($this->_meta[$name]);
        return TRUE;
    }

    public function set_meta_prop($property, $content)
    {
        $this->_meta_prop[$property] = $content;
        return TRUE;
    }
    
    public function set_link($rel, $href)
    {
        $this->_link[] = array ( "rel" => $rel, "href" => $href );
        return TRUE;
    }

    public function set_canonical($url)
    {
       $this->_canonical = $url;
    }

    public function set_amp_canonical($url)
    {
        $this->_amp_canonical = $url;
    }

    public function get_canonical()
    {
       return $this->_canonical;
    }

    public function set_chartset($chartset)
    {
        $this->_charset = $chartset;
        return TRUE;
    }

    public function set_meta_chartset($meta_chartset)
    {
        $this->_meta_charset = $meta_chartset;
        return TRUE;
    }
    
    public function set_language($language)
    {
        $this->_language = $language;
        return TRUE;
    }
    
    public function get_title()
    {
        return $this->_title;
    }

    public function get_h1()
    {
        return $this->_h1;
    }

    public function show()
    {
        $output = "\r\n";
        
        foreach ($this->_meta_http_equiv as $http_equiv => $content) 
        {
            $output .= "\t\t<meta http-equiv=\"{$http_equiv}\" content=\"".$this->_clear_quotes($content)."\"/>\n";
        }

        if (empty($this->_meta_charset) === FALSE)
            $output .= "\t\t<meta charset=\"{$this->_meta_charset}\"/>\n";
        
        $output .= "\t\t<title>{$this->_title}</title>\n";
        
        foreach ($this->_meta as $name => $content) 
        {
            $output .= "\t\t<meta name=\"{$name}\" content=\"".$this->_clear_quotes($content)."\"/>\n";
        }

        foreach ($this->_meta_prop as $property => $content) 
        {
            $item_prop = ($property == 'og:image') ? 'itemprop="image"' : '';
            $output .= "\t\t<meta property=\"{$property}\" {$item_prop} content=\"".$this->_clear_quotes($content)."\"/>\n";
        }

        foreach ($this->_link as $link) 
        {
            if ( empty($link["href"]) === FALSE )
            {
                $output .= "\t\t<link rel=\"{$link['rel']}\" href=\"{$link['href']}\"/>\n";
            }
        }

        if (empty($this->_canonical) === FALSE)
        {
            $output .= "\t\t<link rel=\"canonical\" href=\"{$this->_canonical}\"/>\n";   
        }

        if (empty($this->_amp_canonical) === FALSE)
        {
            $output .= "\t\t<link rel=\"amphtml\" href=\"{$this->_amp_canonical}\"/>\n";   
        }

        return $output;

    }

    private function _clear_quotes($str)
    {
        return htmlentities($str);
    }

}