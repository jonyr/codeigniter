<?php

    function hash_encode($id)
    {
        $ci =& get_instance();
        $hashids = new Hashids\Hashids($ci->config->item('hashids_project'), $ci->config->item('hashids_padding'));
        return $hashids->encode($id);
    }

    function hash_decode($hash)
    {
        $ci =& get_instance();
        $hashids = new Hashids\Hashids($ci->config->item('hashids_project'), $ci->config->item('hashids_padding'));
        return $hashids->decode($hash)[0];
    }